<?php
// Подключаем файлы трейтов
include_once(dirname(__FILE__) . '/trait1.php');
include_once(dirname(__FILE__) . '/trait2.php');
include_once(dirname(__FILE__) . '/trait3.php');
class Test
{
    use Trait1, Trait2, Trait3 {
        /*Указываем, что метод из трейта1 работает вместо 
        методов из трейта2,трейта3. Остальным методам из трейтов 2,3 
        даём псевдонимы и обращаемся к ним*/
        Trait1::method insteadof Trait2,Trait3;
        Trait2::method as method2;
        Trait3::method as method3;
    }

    public static function getSum() {
        return self::method() + self::method2() + self::method3();
    }
}
?>